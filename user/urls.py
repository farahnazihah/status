from django.urls import path
from . import views

app_name = 'books'

urlpatterns = [
    path('', views.home),
    path('logout/', views.logout_oauth, name='logout'),
    path('login/', views.login_page, name='login'),
    path('signup/', views.signup, name='signup')
]