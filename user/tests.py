from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver

from django.apps import apps
from .apps import UserConfig
from .views import login_page, home, signup

# Create your tests here.

class Story10UnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(UserConfig.name, 'user')
        self.assertEqual(apps.get_app_config('user').name, 'user')

    def test_home_url_exist(self):
        response = Client().get('/user/')
        self.assertEqual(response.status_code, 200)

    def test_login_url_exist(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url_exist(self):
        response = Client().get('/user/logout/')
        self.assertEqual(response.status_code, 302)

    def test_signup_url_exist(self):
        response = Client().get('/user/signup/')
        self.assertEqual(response.status_code, 200)

    def test_home_function(self):
        self.found = resolve('/user/')
        self.assertEqual(self.found.func, home)

    def test_signup_function(self):
        self.found = resolve('/user/signup/')
        self.assertEqual(self.found.func, signup)

    def test_login_function(self):
        self.found = resolve('/user/login/')
        self.assertEqual(self.found.func, login_page)

class FuncTest10(LiveServerTestCase):
    def setUp(self):
        opt = Options()
        opt.add_argument('--no-sandbox') 
        opt.add_argument('--headless')
        opt.add_argument('disable-gpu')
        opt.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=opt)
        super(FuncTest10, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FuncTest10, self).tearDown()
    
    def test_search(self):
        self.browser.get(self.live_server_url + "/user/")
        self.browser.implicitly_wait(10)

        # menuju signup page
        signup_page = self.browser.find_element_by_id('signup')
        signup_page.click()
        time.sleep(2)

        # mengisi data untuk sign up
        uname = self.browser.find_element_by_id('username')
        uname.send_keys('canismajoris')
        passw = self.browser.find_element_by_id('password')
        passw.send_keys('sirius')
        signup_btn = self.browser.find_element_by_id('signup')
        signup_btn.click()
        time.sleep(2)

        alert = self.browser.switch_to.alert
        alert.accept()
        time.sleep(2)

        # mengecek username ada di layar
        page_text_signup = self.browser.find_element_by_tag_name('body').text
        self.assertIn('canismajoris', page_text_signup)

        # logout akun barusan
        logout_btn = self.browser.find_element_by_id('logout')
        logout_btn.click()
        time.sleep(2)

        alert = self.browser.switch_to.alert
        alert.accept()
        time.sleep(2)

        # menuju login page
        login_page = self.browser.find_element_by_id('login')
        login_page.click()
        time.sleep(2)

        # mengisi data untuk login
        uname_login = self.browser.find_element_by_id('username')
        uname_login.send_keys('canismajoris')
        passw_login = self.browser.find_element_by_id('password')
        passw_login.send_keys('sirius')
        login_btn = self.browser.find_element_by_id('login')
        login_btn.click()
        time.sleep(2)

        # mengecek username ada di layar
        page_text_login = self.browser.find_element_by_tag_name('body').text
        self.assertIn('canismajoris', page_text_login)