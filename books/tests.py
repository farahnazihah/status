from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver

from django.apps import apps
from .apps import BooksConfig
from .views import books
# Create your tests here.

class story9UnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(BooksConfig.name, 'books')
        self.assertEqual(apps.get_app_config('books').name, 'books')

    def test_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)
    
    def test_books_function(self):
        self.found = resolve('/books/')
        self.assertEqual(self.found.func, books)
    
    def test_intro_is_exist(self):
        response = Client().get('/books/')
        self.assertIn('<h2>some books you might want to read</h2>', response.content.decode())
    
    def test_search_button_is_exist(self):
        self.response = Client().get('/books/')
        self.assertIn('</button>', self.response.content.decode())

class FuncTest9(LiveServerTestCase):
    def setUp(self):
        opt = Options()
        opt.add_argument('--no-sandbox') 
        opt.add_argument('--headless')
        opt.add_argument('disable-gpu')
        opt.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=opt)
        super(FuncTest9, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FuncTest9, self).tearDown()

    def test_search(self):
        self.browser.get(self.live_server_url + "/books/")
        self.browser.implicitly_wait(10)

        search_box = self.browser.find_element_by_id("search-box")
        search_box.send_keys("Harry Potter")
        time.sleep(5)

        search_button = self.browser.find_element_by_id("search-button")
        search_button.click()
        time.sleep(5)

        text = self.browser.find_element_by_id('output').text
        self.assertIn("Harry Potter", text)

    def test_like_book(self):
        self.browser.get(self.live_server_url + "/books/")
        self.browser.implicitly_wait(10)

        search_box = self.browser.find_element_by_id("search-box")
        search_box.send_keys("TFJfCc8Q9GUC")
        search_button = self.browser.find_element_by_id("search-button")
        search_button.click()
        time.sleep(5)

        before = self.browser.find_element_by_class_name("book").text
        like_button = self.browser.find_element_by_class_name("book")
        like_button.click()
        time.sleep(5)
        after = self.browser.find_element_by_class_name("book").text

        self.assertNotEqual(before, after)
    
    def test_top_books(self):
        self.browser.get(self.live_server_url + "/books/")
        self.browser.implicitly_wait(10)

        top_btn = self.browser.find_element_by_id("top5-btn")
        top_btn.click()
        time.sleep(5)

        like = self.browser.find_elements_by_class_name("book")

        like1 = like[0].text
        like2 = like[1].text

        self.assertTrue(like1 >= like2)