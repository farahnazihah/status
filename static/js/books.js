$(document).ready(() => {
    // default keyword
    get_books('Orion');

    $('#search-box').keypress(event => { 
        if(event.which == 13) {
            $('#search-button').click(); 
        }
    });

    // search keyword
    $('#search-button').on('click', event => {
        let keyword = $('#search-box').val();
        console.log("keyword")
        if(keyword == '') keyword = 'Orion';
        console.log(keyword)
        get_books(keyword);
    });


    // add like
    $("#output").on("click", ".book", (function(e) {
        console.log(e.target.id);
        var csrftoken = $('[name="csrfmiddlewaretoken"]').attr('value')
        $.ajax({
            type: "POST",
            url: "/books/books.json",
            data: {"id_buku":e.target.id},
            dataType: 'json',
            headers: {
                "X-CSRFToken": csrftoken,
                },
            success: response => {
                console.log(response)
                console.log(response.items[0].likes);
                document.getElementById(e.target.id).innerHTML = response.items[0].likes;
            }
        })
    }));
    
    $('#top5-btn').on('click', (event=> {
        top_books()
    }));

    // top books
    function top_books() {
        $('#output').html(
            `
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Cover</th>
                    <th scope="col">Title</th>
                    <th scope="col">Author(s)</th>
                    <th scope="col">Likes</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            `);

            var csrftoken = $('[name="csrfmiddlewaretoken"]').attr('value')
            $.ajax({
                type: "GET",
                url: "/books/top-books",
                headers: {
                    "X-CSRFToken": csrftoken,
                    },
                success: response => {
                    let number = 1;
                    response.items.forEach(obj => {
                        $('tbody').append(
                            $('<tr/>').append(
                                $('<td/>').html(number)
                            ).append(
                                $('<td/>').append(
                                    $('<img/>').attr('src', obj.cover)
                                )
                            ).append(
                                $('<td/>').html(obj.title)
                            ).append(
                                $('<td/>').html(obj.authors)
                            ).append(
                                $('<td/>').append(
                                    $('<button/>').html(obj.likes).attr('id', obj.id).attr('class', 'btn btn-info book')
                                )
                            )
                        );
    
                        number++;
                    });
                }
            })
    }
    // fetch data from views
    function get_books(keyword) {
        $('#output').html(
        `
        <table class="table">
            <thead class="thead-dark">
                <tr>
                <th scope="col">#</th>
                <th scope="col">Cover</th>
                <th scope="col">Title</th>
                <th scope="col">Author(s)</th>
                <th scope="col">Likes</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        `);

        $.ajax({
            type: "GET",
            url: "/books/books.json?keyword=" + keyword,
            success: response => {
                let number = 1;
                response.items.forEach(obj => {
                    $('tbody').append(
                        $('<tr/>').append(
                            $('<td/>').html(number)
                        ).append(
                            $('<td/>').append(
                                $('<img/>').attr('src', obj.cover)
                            )
                        ).append(
                            $('<td/>').html(obj.title)
                        ).append(
                            $('<td/>').html(obj.authors)
                        ).append(
                            $('<td/>').append(
                                $('<button/>').html(obj.likes).attr('id', obj.id).attr('class', 'btn btn-info book')
                            )
                        )
                    );

                    number++;
                });
            }
        })
    }
});
