$(document).ready(function() {
  $('.acc-head > button').on('click', function(){
      if($(this).hasClass('active')) {
          $(this).removeClass('active');
          $(this).siblings('.content').slideUp(200);
          $('.acc-head > button i').removeClass("fa-minus").addClass('fa-plus');
      } else {
          $('.acc-head > button i').removeClass('fa-minus').addClass('fa-plus');
          $(this).find('i').removeClass('fa-plus').addClass('fa-minus');
          $('.acc-head > button').removeClass('active');
          $(this).addClass('active');
          $('.content').slideUp(200);
          $(this).siblings('.content').slideDown(200);
      }
  });
  $('.down').click(function (e) {
      var self = $(this),
          item = self.parents('div#accordion'),
          swapWith = item.next();
      item.before(swapWith.detach());
  });
  $('.up').click(function (e) {
      var self = $(this),
          item = self.parents('div#accordion'),
          swapWith = item.prev();
      item.after(swapWith.detach());
  });
});