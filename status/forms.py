from django.forms import ModelForm, forms, Textarea
from .models import Status

class StatusForm(ModelForm):
    class Meta:
        model = Status
        fields = ['nama', 'pesan']