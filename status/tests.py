from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from django.http import HttpRequest
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver

from .views import status, konfirmasi
from .apps import StatusConfig
from .models import Status


# Create your tests here.

class UnitTest(TestCase):
	def test_apps(self):
		self.assertEqual(StatusConfig.name, 'status')
		self.assertEqual(apps.get_app_config('status').name, 'status')

	def test_homepage_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_confirmation_url_is_exist(self):
		response = Client().get('/confirm/')
		self.assertEqual(response.status_code, 200)

	def test_form_function(self):
		self.found = resolve('/')
		self.assertEqual(self.found.func, status)

	def test_confirm_function(self):
		self.found = resolve('/confirm/')
		self.assertEqual(self.found.func, konfirmasi)

	def test_activity_model_create_new_object(self):
		status = Status(nama="Rigel", pesan="Not really good.", color="#999999")
		status.save()
		self.assertEqual(Status.objects.all().count(), 1)

	def test_create_models(self):
		before = Status.objects.all().count()
		Status.objects.create(nama="Pollux", pesan="Thinking.", color="#999999")
		after = Status.objects.all().count()
		self.assertNotEqual(before, after)

	def test_intro_is_exist(self):
		response = Client().get('/')
		self.assertIn('<h1>Apa kabar?</h1>', response.content.decode())
	
	def test_form_is_exist(self):
		self.response = Client().get('/')
		self.assertIn('</form>', self.response.content.decode())

	def test_response_redirect(self):
		response = Client().post('/', {'nama':'Mintaka', 'pesan':'Not really good.'})
		self.assertEqual(response.status_code, 302)

	def test_change_color_status(self):
		status = Status(nama="rigel lagi", pesan="cape bgt sumpa", color="ffffff")
		status.save()
		response = Client().post('/color/1')
		self.assertEqual(response.status_code, 301)
	


class FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(FunctionalTest, self).tearDown()

	def test_confirmed_input_form(self):
		self.browser.get(self.live_server_url)
		time.sleep(5)
		
		name = self.browser.find_element_by_name('nama')
		name.send_keys('Betelgeuse')
		
		message = self.browser.find_element_by_name('pesan')
		message.send_keys('Not gonna come back.')

		time.sleep(5)

		submit = self.browser.find_element_by_id('submit')
		submit.click()

		time.sleep(5)

		self.assertIn('Apakah anda yakin ingin memposting status berikut?', self.browser.page_source)
		yes = self.browser.find_element_by_id('ya')
		yes.click()

		self.assertIn('Betelgeuse', self.browser.page_source)
		self.assertIn('Not gonna come back.', self.browser.page_source)

	def test_canceled_input_form(self):
		self.browser.get(self.live_server_url)
		time.sleep(5)
		
		name = self.browser.find_element_by_name('nama')
		name.send_keys('betaoionis')
		
		message = self.browser.find_element_by_name('pesan')
		message.send_keys('Must I die before you feel alive?~')

		time.sleep(5)

		submit = self.browser.find_element_by_id('submit')
		submit.click()

		time.sleep(5)

		self.assertIn('Apakah anda yakin ingin memposting status berikut?', self.browser.page_source)
		no = self.browser.find_element_by_id('no')
		no.click()

		self.assertNotIn('betaorionis', self.browser.page_source)
		self.assertNotIn('Must I die before you feel alive?~', self.browser.page_source)

	def test_change_status_color(self):
		status = Status(nama="rigel lagi", pesan="cape bgt sumpa", color="ffffff")
		status.save()

		self.browser.get(self.live_server_url)
		before = self.browser.find_element_by_class_name('card').value_of_css_property('background-color')
		
		gantiButton = self.browser.find_element_by_name('ganti')
		gantiButton.click()

		after = self.browser.find_element_by_class_name('card').value_of_css_property('background-color')

		self.assertNotEqual(before, after)