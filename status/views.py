from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Status
from .forms import StatusForm
import random

# Create your views here.

def status(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            request.session['nama'] = form.data['nama']
            request.session['pesan'] = form.data['pesan']
            return redirect('/confirm')
    else:
        form = StatusForm()
        
    context = {
        'status' : Status.objects.all(),
        'form' : form
    }

    return render(request, 'home.html', context)

def colorChange(request, index):
    if request.method == "POST":
        red = (random.randint(150, 250))
        green = (random.randint(150, 250))
        blue = (random.randint(150, 250))
        color = "#%02x%02x%02x" % (red, green, blue)
        obj = Status.objects.get(pk=index)
        obj.color = color
        obj.save()

    return redirect('/')

def konfirmasi(request):
    if request.method == 'POST':
        nama = request.session.get('nama')
        pesan = request.session.get('pesan')
        warna = "#ffffff"
        new_status = Status(
                nama = nama,
                pesan = pesan,
                color = warna
            )

        if nama != None and pesan != None:
            new_status.save()
            del request.session['nama']
            del request.session['pesan']
        
        return redirect('/')
    
    context = {
        'nama' : request.session.get('nama'),
        'pesan' : request.session.get('pesan')
    }

    return render(request, 'confirm.html', context)
